package com.example.vaadindemo;



import com.example.vaadindemo.domain.Message;
import com.example.vaadindemo.domain.Person;
import com.example.vaadindemo.service.MessageMenager;
import com.example.vaadindemo.service.PersonManager;
import com.vaadin.annotations.Title;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@Title("Vaadin Demo App")
public class VaadinApp extends UI {

	private PersonManager personManager = new PersonManager();
	private MessageMenager messageManager = new MessageMenager();
	
	TextField def = new TextField("Dodaj Zapis");
	Label login = new Label();
	
	String systemMessage = new String();
    
	private Person user = new Person("");
	private Message message = new Message("Hello", user); 
	
	private BeanItem<Person> personItem = new BeanItem<Person>(user);
	private BeanItem<Message> messageItem = new BeanItem<Message>(message);

	private BeanItemContainer<Person> persons = new BeanItemContainer<Person>(
			Person.class);
	private BeanItemContainer<Message> messages = new BeanItemContainer<Message>(
			Message.class);
	
	enum Action {
		EDIT, ADD, LOG, ERR;
	}
	
	private class LogInWindow extends Window {
		private static final long serialVersionUID = 1L;
		
		private Action action;

		public LogInWindow(Action act) {
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case LOG:
				setCaption("Zaloguj sie");
				break;
			default:
				break;
			}
		final FormLayout form = new FormLayout();
		final FieldGroup binder = new FieldGroup(personItem);

		final Button logBtn = new Button(" Log in ");
		final Button cancelBtn = new Button(" Anuluj ");
			               	      
		form.addComponent(binder.buildAndBind("Nick", "firstName"));
		
		binder.setBuffered(true);
		binder.getField("firstName").setRequired(true);
			
		binder.getField("firstName").addValidator(

                new StringLengthValidator(
                     "Must be between 2 and 50 characters in length", 2, 50, false));
		

		VerticalLayout fvl = new VerticalLayout();
		fvl.setMargin(true);
		fvl.addComponent(form);

		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(logBtn);
		hl.addComponent(cancelBtn);
		fvl.addComponent(hl);

		setContent(fvl);

		logBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				try {
					binder.commit();

					if (action == Action.LOG) {
						personManager.addPerson(user);
					}				
					persons.removeAllItems();
					persons.addAll(personManager.findAll());
					login.setCaption("Zalogowany jako " + user.getFirstName());
					close();
				} catch (CommitException e) {
					e.printStackTrace();
				}
			}
		});

		cancelBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				binder.discard();
				close();
			}
		});
	}
		
	}
	
	
	private class ErrorWindow extends Window {
		private static final long serialVersionUID = 1L;
		
		private Action action;

		public ErrorWindow(Action act) {
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case ERR:
				setCaption("Error");
				break;
			default:
				break;
			}
			
		final Button cancelBtn = new Button(" OK ");
		final Label sys = new Label(systemMessage);
			
		VerticalLayout fvl = new VerticalLayout();
		fvl.setMargin(true);
		fvl.addComponent(sys);
		
		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(cancelBtn);
		fvl.addComponent(hl);

		setContent(fvl);

		cancelBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
	}
		
	}
	
	private class MyFormWindow extends Window {
		private static final long serialVersionUID = 1L;

		private Action action;

		public MyFormWindow(Action act) {
			this.action = act;

			setModal(true);
			center();
			
			switch (action) {
			case ADD:
				setCaption("Dodaj nowy zapis");
				break;
			default:
				break;
			}
			
			final FormLayout form = new FormLayout();
			final FieldGroup binder = new FieldGroup(messageItem);

			final Button saveBtn = new Button(" Dodaj ");
			final Button cancelBtn = new Button(" Anuluj ");
				               	      
			form.addComponent(new TextArea(binder.buildAndBind("Message", "message")));
			

			binder.setBuffered(true);
			
			binder.getField("message").setRequired(true);
			binder.getField("message").addValidator(  
					
					new StringLengthValidator(
                    "Add your message. 500 charecters max", 1, 500, false));

			VerticalLayout fvl = new VerticalLayout();
			fvl.setMargin(true);
			fvl.addComponent(form);

			HorizontalLayout hl = new HorizontalLayout();
			hl.addComponent(saveBtn);
			hl.addComponent(cancelBtn);
			fvl.addComponent(hl);

			setContent(fvl);

			saveBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						binder.commit();

						if (action == Action.ADD) {
							messageManager.addMessage(message, user);
						} else if(action == Action.EDIT){
							messageManager.updateMessage(message);
						}
						messages.removeAllItems();
						messages.addAll(messageManager.findAll());
						close();
					} catch (CommitException e) {
						e.printStackTrace();
					}
				}
			});

			cancelBtn.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.discard();
					close();
				}
			});
		}
	}
		
	protected void init(VaadinRequest request)
	{
		    
		Button addMessageBtn = new Button("Add message ");
		Button editMessageBtn = new Button("Edit message ");
		Button confirmEditBtn = new Button("Confirm");
		Button cencelEditBtn = new Button("Cencel");
		Button Login = new Button("Zaloguj ");
		Button Logout = new Button("Wyloguj ");
		Button deleteMessageBtn = new Button("Delete message");
		TextArea edit = new TextArea();
		

		VerticalLayout vl = new VerticalLayout();
		setContent(vl);
		
		Grid grid = new Grid(messages);
		grid.setWidth("850px");
		grid.setHeight("500px");
		
		HeaderRow filterRow = grid.appendHeaderRow();
		
		for (Object pid: grid.getContainerDataSource()
                .getContainerPropertyIds()) {
HeaderCell cell = filterRow.getCell(pid);

// Have an input field to use for filter
TextField filterField = new TextField();
filterField.setColumns(4);

// Update filter When the filter input is changed
filterField.addTextChangeListener(change -> {
   // Can't modify filters so need to replace
	messages.removeContainerFilters(pid);

   // (Re)create the filter if necessary
   if (! change.getText().isEmpty())
	   messages.addContainerFilter(
           new SimpleStringFilter(pid,
               change.getText(), true, false));
});
cell.setComponent(filterField);
}
		
		grid.setColumnOrder("authorNick", "message", "date");
		grid.removeColumn("id");
		grid.setFrozenColumnCount(3);
		grid.getColumn("authorNick").setMinimumWidth(200);
		grid.getColumn("message").setMinimumWidth(500);
		grid.getColumn("date").setMinimumWidth(150);
		
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.getColumn("authorNick").setEditable(false);
		grid.getColumn("date").setEditable(false);
		
		
						
		addMessageBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				if(user.getFirstName() == ""){
					systemMessage = "Log In first";
					addWindow(new LogInWindow(Action.LOG));	
					addWindow(new ErrorWindow(Action.ERR));	
				}else{
	
				addWindow(new MyFormWindow(Action.ADD));
				}
			}
		});
				
		Login.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				addWindow(new LogInWindow(Action.LOG));		
			}
		});
		
		
		Logout.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				user.setFirstName("");
				login.setCaption("Wylogowany" + user.getFirstName());
			}
		});
		
				
		deleteMessageBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				Message mes = (Message)grid.getSelectedRow();
				
				if (mes.getAuthorNick().equals(user.getFirstName())) {
					
				       	   if (!mes.getAuthorNick().isEmpty()) {
				    	   messageManager.delete(mes);
				    	   messages.removeAllItems();
				    	   messages.addAll(messageManager.findAll());
				    	   grid.clearSortOrder();				
				      }
				 }
			}
		});
		
		HorizontalLayout h1 = new HorizontalLayout();
		h1.addComponent(Login);
		h1.addComponent(Logout);
			
		HorizontalLayout h2 = new HorizontalLayout(); 
		VerticalLayout v3 = new VerticalLayout();
		HorizontalLayout h4 = new HorizontalLayout(); 
		
		v3.addComponent(editMessageBtn);
	    v3.addComponent(h4);
		h2.addComponent(grid);
		h2.addComponent(v3);
		
		HorizontalLayout h3 = new HorizontalLayout(); 
		h3.addComponent(addMessageBtn);
		h3.addComponent(deleteMessageBtn);
		
	editMessageBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				
				Message mes = (Message)grid.getSelectedRow();
				
				if (mes.getAuthorNick().equals(user.getFirstName())) {
					
					
				       	   if (!mes.getAuthorNick().isEmpty()) {
				       		   
				       		   edit.setValue(mes.getMessage());
				       		   
				       		   v3.removeComponent(editMessageBtn);				       		
				       		   v3.addComponent(edit);
				       		   h4.addComponent(confirmEditBtn);
				       		   h4.addComponent(cencelEditBtn);
				       	   		       		   				       		   			       		   
				      }
				 }
			}
		});
		
		confirmEditBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				
				Message mes = (Message)grid.getSelectedRow();
				
				if (mes.getAuthorNick().equals(user.getFirstName())) {
					
					
				       	   if (!mes.getAuthorNick().isEmpty()) {
				       		  
				       		   String edited = edit.getValue();			       		  	       		  
				       		   mes.setMessage(edited);
				       		   messageManager.updateMessage(mes);
				       		   messages.removeAllItems();
							   messages.addAll(messageManager.findAll());
				       		   grid.clearSortOrder();
				       		   
				       		v3.addComponent(editMessageBtn);   
				       		v3.removeComponent(edit);
				       		h4.removeComponent(confirmEditBtn);
				       		h4.removeComponent(cencelEditBtn);
				       		
				       		   
				      }
				 }
			}
		});
		
		cencelEditBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
							       		   	
				       		v3.addComponent(editMessageBtn);   
				       		v3.removeComponent(edit);
				       		h4.removeComponent(confirmEditBtn);
				       		h4.removeComponent(cencelEditBtn);				       		
				       		   
				      }
				 
		});
		
		vl.addComponent(login);
		vl.addComponent(h1);
		vl.addComponent(h2);
		vl.addComponent(h3);
		
							
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		Label label = new Label();
		horizontalLayout.addComponent(label);
		label.setValue(UI.getCurrent().toString());
		
		vl.addComponent(horizontalLayout);
             
    }	
	
}