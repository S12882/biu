package com.example.vaadindemo;
import java.util.Enumeration;

import com.vaadin.annotations.Title;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class Clicker extends Button {
	
	public Button button;
	public TextField firstNum;
	public TextField secondNum;
	public Label resultLabel;
	public enum Action{Plus, Minus, Multi};
	
	
	public Clicker(TextField firstNum, TextField secondNum)
	{	
		this.firstNum = firstNum;
		this.secondNum = secondNum;
	}
	
	public Clicker()
	{	
		
	}
	
	public String ClickAnswer(Button button, TextField firstNum, TextField secondNum) 
	{	
		
				String answer; 	
				float result = 2;
				float x = Float.parseFloat(firstNum.getValue());
				float y = Float.parseFloat(secondNum.getValue());	   
				
		     	
		     	if(button.getCaption()=="+"){
		     		 result=Plus(x, y);
		     	} 
		     	     	
		     	if(button.getCaption()=="-"){
		     		 result=Minus(x, y);
		     	}
		     		     	
		     	if(button.getCaption()=="*"){
		     		 result=Multi(x, y);
		     	}
		     	
		    	if(button.getCaption()=="/"){
		     		 result=Div(x, y);
		     	}
		     	
     
		     	answer = Float.toString(result);				
				return answer;

	}
	
	public float Plus(float firstNum, float secondNum){
		
		float result;
		result = firstNum+secondNum;
		
	 return result;	
	}
	
public float Minus(float firstNum, float secondNum){
		
		float result;
		result = firstNum-secondNum;
		
		return result;	
	}
		
public float Multi(float firstNum, float secondNum){
	
		float result;
		result = firstNum*secondNum;
	
	return result;	
}

public float Div(float firstNum, float secondNum){
	
		float result;
		result = firstNum/secondNum;
	
	return result;	
}
		

}
