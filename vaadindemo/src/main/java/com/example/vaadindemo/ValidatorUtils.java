package com.example.vaadindemo;

import java.util.Collection;

import com.example.vaadindemo.domain.Person;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.ui.Field;

public class ValidatorUtils {

	private ValidatorUtils() {} 
	
	static void installSingleValidator(Field<?> field, String attribute) {
		
		Collection<Validator> validators = field.getValidators();

		if (validators == null || validators.isEmpty()) {

			field.addValidator(new BeanValidator(Person.class, attribute));
		}
	}
}