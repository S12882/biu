package com.example.vaadindemo.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.example.vaadindemo.domain.Message;
import com.example.vaadindemo.domain.Person;

public class MessageMenager {
	
	private List<Message> mb = new ArrayList<Message>();
	
 
	public void addMessage(Message message, Person author){
		Message m = new Message(message.getMessage(), author.getFirstName());
		m.setId(UUID.randomUUID());
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		String data = dateFormat.format(date);
		
		m.setDate(data);
		mb.add(m);
	}
	
	public List<Message> findAll(){
		return mb;
	}

	public void delete(Message message) {
		
		Message toRemove = null;
		for (Message p : mb) {
			if (p.getId().equals(message.getId())){
				toRemove = p;
				break;
			}
		}
		mb.remove(toRemove);		
	}

	public void updateMessage(Message message) {
		
		for(Message mes : mb) {
	        if(mes.getId().equals(message.getId())) {	        	
	        	mes.setMessage(message.getMessage());
	        	mes.setAuthor(message.getAuthor());
	        	break;
	        }
	    }
		
	}

}