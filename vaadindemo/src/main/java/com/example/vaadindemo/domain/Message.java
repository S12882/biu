package com.example.vaadindemo.domain;


import java.util.UUID;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Message {
	
	private UUID id;
	
	@Size(min = 1, max = 500)
	@NotNull
	private String message;
	private Person author;
	private String authorNick;
	private String data;

	public Message(String message, Person author) {
		super();
		this.message = message;
		this.author = author;
	}
	
	public Message(String message, String authorNick) {
		super();
		this.message = message;
		this.authorNick = authorNick;
	}
	
	public Message(String message) {
		super();
		this.message = message;
	}
	
	public Message() {
	}
	
	public Person getAuthor() {
		return author;
	}
	
	public void setAuthor(Person author) {
		this.author = author;
	}
	
	public String getAuthorNick() {
		return authorNick;
	}

	public void setAuthorNick(Person author) {
		String authorNick = author.getFirstName();
		this.authorNick = authorNick;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	

	public void setDate(String data) {
		this.data = data;
	}
	
	public String getDate() {
		return data;
	}
	
	
	
}