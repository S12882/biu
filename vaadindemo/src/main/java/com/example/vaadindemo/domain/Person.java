package com.example.vaadindemo.domain;


import java.util.UUID; 
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
 

public class Person {
	
	private UUID id; 
	
	@Size(min = 3, max = 50)
	@NotNull
	private String firstName; 
	

	public Person(String firstName) {
		super();
		this.firstName = firstName;	 
	}
	
	public Person() {
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Override  
	public String toString() {
		return "Person [firstName=" + firstName + "]";			
	} 

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
}