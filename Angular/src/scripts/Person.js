"use strict";
// export - klasa publiczna
var Person = (function () {
    function Person(json) {
		this.id = json.id;
        this.name = json.firstName;
        this.lastName = json.lastName;
        this.gender = json.gender;
        this.email = json.email;
        this.income = json.income;
        this.birthdate = json.birthsday;
        this.age = json.age;
    }
    Person.prototype.toTableRow = function () {
        return '<tr><td>'
		    + this.id
            + '</td><td>'
            + this.name
            + '</td><td>'
            + this.lastName
            + '</td><td>'
            + this.gender
            + '</td><td>'
            + this.email
            + '</td><td>'
            + this.age
            + '</td><td>'
            + this.birthdate
            + '</td><td>'
            + this.income
            + '</td></tr>';
    };
    Person.prototype.fullName = function () {
        return this._name + this._lastName;
    };
    Object.defineProperty(Person.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (v) {
            this._name = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "lastName", {
        get: function () {
            return this._lastName;
        },
        set: function (v) {
            this._lastName = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "gender", {
        get: function () {
            return this._gender;
        },
        set: function (v) {
            this._gender = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (v) {
            this._email = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "income", {
        get: function () {
            return this._income;
        },
        set: function (v) {
            this._income = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "age", {
        get: function () {
            return this._age;
        },
        set: function (v) {
            this._age = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "birthdate", {
        get: function () {
            return this._birthdate;
        },
        set: function (v) {
            this._birthdate = v;
        },
        enumerable: true,
        configurable: true
    });
    return Person;
}());
exports.Person = Person;
// zmienna glob
exports.test = "";
//# sourceMappingURL=person.js.map