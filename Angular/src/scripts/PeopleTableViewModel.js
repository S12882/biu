function PeopleTableViewModel(config){
    var self = this;
    self.people = new ListOfPeople();
    self.currentPage = 0;
    self.pageSize = 30;
    self.context = config.context;

    self.next = function(){
        self.people.clear();
        var begin = (self.currentPage)*self.pageSize;
        var end =(self.currentPage+1)*self.pageSize;
        getData(begin,end);
        self.currentPage++;
        refreshTable();
    };

    self.prev = function() {
        self.people.clear();
        if (self.currentPage - 1 >= 0) {
            self.currentPage--;
        }
        var begin = (self.currentPage) * self.pageSize;
        var end = (self.currentPage + 1) * self.pageSize;
        getData(begin, end);
        refreshTable();
    };

    self.sort = function(comparer){
        data.sort(comparer);
        self.currentPage=0;
        self.next();
    };

    function search() {
        var table, tr, td1, td2, td3, i, filter1, filter2, filter3, filter4, search1, search2, search3, search4;

        filter1 = document.getElementById("name_filter");
        filter2 = document.getElementById("surname_filter");
        filter3 = document.getElementById("age_filter");
        filter4 = document.getElementById("id_filter");

        search1 = filter1.value.toUpperCase();
        search2 = filter2.value.toUpperCase();
        search3 = filter3.value.toUpperCase();
        search4 = filter4.value.toUpperCase();

        table = document.getElementById("table");
        tr = table.getElementsByTagName("tr");

        for (i = 0; i < tr.length; i++) {
            td1 = tr[i].getElementsByTagName("td")[1];
            td2 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[5];
            td4 = tr[i].getElementsByTagName("td")[0];

            if (td1 || td2 || td3 || td4) {
                if ((td1.innerHTML.toUpperCase().indexOf(search1) > -1) && (td2.innerHTML.toUpperCase().indexOf(search2) > -1) && (td3.innerHTML.toUpperCase().indexOf(search3) > -1) &&
                    (td4.innerHTML.toUpperCase().indexOf(search4) > -1)){
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }

    }


    var getData = function(begin, end){
        if(end>data.length){
            end=data.length
        }
        if(begin<0) {
            begin =0;
        }
        for(var i = begin; i<end;i+=1){
            self.people.addPerson(data[i]);
        }
    };

    var refreshTable = function(){
        self.context.html(self.people.toTable());

        $('.js-sort-name').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.byname);
        });

        $('.js-sort-surname').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.bySurname);
        });

        $('.js-sort-id-r').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.byIdR);
        });

        $('.js-sort-id-d').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.byIdD);
        });


        $('.js-sort-age-r').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.byAgeR);

        });

        $('.js-sort-age-d').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.byAgeD);
        });

        $('.js-sort-income-r').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.byIncomeR);
        });

        $('.js-sort-income-d').click(function(){
            var comparator = new Comparators();
            self.sort(comparator.byIncomeD);
        });


        $('#checkbox').change(function() {
            if ($(this).is(":checked")) {

                $('td:nth-child(8),th:nth-child(8)').hide();

            } else {
                $('td:nth-child(8),th:nth-child(8)').show();
            }
        });

        $('.js-search-bt').click(function(){
           search();
        });

    }
}