function ListOfPeople(){
    var people =[];
    var self = this;
    self.addPerson = function(json){
        people.push(new Person(json));
    }
    
    self.toTable = function(){
        var table = '<table class="table table-striped table-hover table-bordered">';
        table += generateTableHeader();
        for(var i =0;i<people.length;i++){
            table+=people[i].toTableRow();
        }
        table+='</table>'
        return table;
    }
    
    self.clear = function(){
        people = [];
    }

    var generateTableHeader= function(){
        return '<tr><th>Id</th>'
            +' <th><button  class="btn btn-sm btn-danger js-sort-name">Name</button></th>'
            +' <th><button  class="btn btn-sm btn-danger js-sort-surname">Surname</button></th>'
            +' <th>Gender</th>'
            +' <th>Email</th>'
            +' <th>Age <button  class="btn btn-sm btn-danger js-sort-age-r"> + </button> ' +
            '<button  class="btn btn-sm btn-danger js-sort-age-d"> - </button> </th>'
            +' <th>Birthsday</th>'
            +' <th>Income <button  class="btn btn-sm btn-danger js-sort-income-r"> + </button> ' +
            '<button  class="btn btn-sm btn-danger js-sort-income-d"> - </button></th>'
            +'</tr>'
    }
}