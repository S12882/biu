import { Component, Input } from '@angular/core';
import {Comp} from "../comp";

@Component({
  selector: 'app-comp-list',
  templateUrl: './comp-list.component.html',
  styleUrls: ['./comp-list.component.css'],
})
export class CompListComponent implements Input {
  selectedComp: Comp;
  comps: Comp[] = [{ id: 1, name: 'Lenovo', likes: Math.floor(Math.random() * 10) + 1 , voted: false},
    { id: 2, name: 'MSI', likes: Math.floor(Math.random() * 10) + 1, voted: false},
    { id: 3, name: 'hp', likes: Math.floor(Math.random() * 10) + 1, voted: false},
    { id: 4, name: 'Acer', likes: Math.floor(Math.random() * 10) + 1, voted: false},
    { id: 5, name: 'IBM', likes: Math.floor(Math.random() * 10) + 1, voted: false}
  ];

  onSelect(comp: Comp): void {
    this.selectedComp = comp;
  }
}
