import {Pipe, PipeTransform} from '@angular/core';
import {Comp} from "./comp";

@Pipe({name: 'Sort', pure: false})
export class Sort implements PipeTransform {

  transform(array: Array<Comp>, args: string): Array<Comp> {
    array.sort((A: any, B: any) => {
      if (A.likes > B.likes) {
        return -1;
      } else if (A.likes < B.likes) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
