import {Component, Input} from '@angular/core';
import {Comp} from "../comp";

@Component({
  selector: 'app-comp',
  templateUrl: './comp.component.html',
  styleUrls: ['./comp.component.css']
})

export class CompComponent implements Input {
  @Input()
  comp: Comp;
  addLike():void{
    if(!this.comp.voted){
      this.comp.likes+=1;
      this.comp.voted=true;
    }
  }
  subLike():void{
    if(!this.comp.voted){
      this.comp.likes-=1;
      this.comp.voted=true;
    }
  }

}
