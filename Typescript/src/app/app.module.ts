import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CompListComponent } from './comp-list/comp-list.component';
import { CompComponent } from './comp/comp.component';
import {Sort} from "./Sort";

@NgModule({
  declarations: [ 
    AppComponent,
    CompListComponent,
    CompComponent,
    Sort
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
