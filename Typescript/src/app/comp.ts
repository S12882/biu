export class Comp {
  id: number;
  name: string;
  likes: number;
  voted: boolean;
}
